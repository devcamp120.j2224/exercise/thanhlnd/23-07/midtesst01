package com.devcamp.rest_api;

import java.util.ArrayList;

public class Department {
    int id;
    String name;
    String address;
    ArrayList<Staff> staffs;

    public Department() {
    }

    public Department(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Department(int id, String name, String address, ArrayList<Staff> staffs) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.staffs = staffs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Staff> getStaffs() {
        return staffs;
    }

    public void setStaffs(ArrayList<Staff> staffs) {
        this.staffs = staffs;
    }

    public float getAverageAge() {
        float sum = 0;
        for (int i = 0; i < this.staffs.size(); i++) {
            sum +=  staffs.get(i).getAge();
        }
        return sum / staffs.size();
    }

    @Override
    public String toString() {
        return "Department [address=" + address + ", id=" + id + ", name=" + name + ", staffs=" + getAverageAge() + "]";
    }
    
}
